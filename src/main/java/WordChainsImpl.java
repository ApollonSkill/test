import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class WordChainsImpl implements WordChains {

    private String pathToTheDictionary;
    private static final Integer ZERO = 0;
    private static final Integer ONE = 1;

    public WordChainsImpl(String pathToTheDictionary) {
        this.pathToTheDictionary = pathToTheDictionary;
    }

    public String getPathToTheDictionary() {
        return pathToTheDictionary;
    }

    public void setPathToTheDictionary(String pathToTheDictionary) {
        this.pathToTheDictionary = pathToTheDictionary;
    }

    public List<String> getWordChains(String startWord, String endWord) {
        Map<Integer,  Set<String>> sortedDictionaryWords = getSortedDictionaryWords();
        List<List<String>> possibleChains = new ArrayList<>();
        List<List<String>> resultChains = new ArrayList<>();
        if (startWord.length() == endWord.length()) {
            Integer wordLength = startWord.length();
                if (sortedDictionaryWords.containsKey(wordLength)) {
                    Set<String> certainLengthWords = sortedDictionaryWords.get(wordLength);
                    createFirstChain(certainLengthWords, startWord, endWord, possibleChains);
                    createNextChains(certainLengthWords, endWord, possibleChains, resultChains);
                }
        }
        return searchCorrectChainWithMinCountElements(resultChains, endWord);
    }
    private boolean finishedChain(String lastWord, String endWord) {
        int mistakesAllowed = ONE;
        for(int i = ZERO; i < lastWord.length(); i++) {
            if(lastWord.charAt(i) != endWord.charAt(i)) {
                mistakesAllowed--;
                if(mistakesAllowed < ZERO) {
                    return false;
                }
            }
        }
        return true;
    }

    private void createFirstChain(Set<String> certainLengthWords, String startWord, String endWord, List<List<String>> possibleChains) {
        for (String certainLengthWord : certainLengthWords) {
            if (equals(certainLengthWord, startWord) && !startWord.equals(certainLengthWord) && !endWord.equals(certainLengthWord)) {
                List<String> possibleChain = new ArrayList<>();
                possibleChain.add(ZERO, startWord);
                possibleChain.add(certainLengthWord);
                possibleChains.add(possibleChain);
            }
        }
    }

    private void createNextChains(Set<String> certainLengthWords, String endWord, List<List<String>> possibleChains, List<List<String>> resultChains) {
        for (String certainLengthWord : certainLengthWords) {
            for (int i = 0; i < possibleChains.size(); i++) {
                List<String> possibleChain = possibleChains.get(i);
                String lastElement = possibleChain.get(possibleChains.get(i).size() - ONE);
                if (equals(certainLengthWord, lastElement) && !possibleChain.contains(certainLengthWord) && !endWord.equals(certainLengthWord)) {
                    possibleChain.add(certainLengthWord);
                    if (finishedChain(certainLengthWord, endWord)) {
                        possibleChain.add(endWord);
                        resultChains.add(possibleChain);
                        possibleChains.remove(possibleChain);
                    } else {
                        createNextChains(certainLengthWords, endWord, possibleChains, resultChains);
                    }
                }
            }
        }
    }

    private List<String> formatResultChain(String startWord, String endWord, List<String> resultCain) {
        if (resultCain != null) {
            resultCain.add(ZERO, startWord);
            resultCain.add(endWord);
        }
        return resultCain;
    }

    private boolean equals(String word, String lastChainWord) {
        //if(word1.equals(word2)) return false;
        int mistakesAllowed = ONE;
        for (int i = ZERO; i < lastChainWord.length(); i++) {
            if (word.charAt(i) != lastChainWord.charAt(i)) {
                mistakesAllowed--;
                if (mistakesAllowed < ZERO) {
                    return false;
                }
            }
        }
        return true;
    }

    private List<String> searchCorrectChainWithMinCountElements(List<List<String>> possibleChains, String endWord) {
        List<String> minChain = null;
        int minChainSize = Integer.MAX_VALUE;
        for (List<String> possibleChain: possibleChains) {
            String lastWord = possibleChain.get(possibleChain.size() - 1);
            if (endWord.equals(lastWord) && minChainSize > possibleChain.size()) {
                minChain = possibleChain;
            }
        }
        return minChain;
    }


    private Map<Integer, Set<String>> getSortedDictionaryWords() {
        Map<Integer, Set<String>> sortedDictionaryWords = new HashMap<>();
        Set<String> dictionaryWords = getDictionaryWords();
        for (String word: dictionaryWords) {
            Integer wordLength = word.length();
            if (sortedDictionaryWords.containsKey(wordLength)) {
                Set<String> certainLengthWords = sortedDictionaryWords.get(wordLength);
                certainLengthWords.add(word);
            } else {
                Set<String> certainLengthWords = new HashSet<>();
                certainLengthWords.add(word);
                sortedDictionaryWords.put(wordLength, certainLengthWords);
            }
        }
        return sortedDictionaryWords;
    }

    private Set<String> getDictionaryWords() {
        Set<String> words = new HashSet<>();
        try (BufferedReader br = new BufferedReader(new FileReader(pathToTheDictionary))) {
            String word;
            while ((word = br.readLine()) != null) {
                words.add(word.trim().toLowerCase());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return words;
    }
}
