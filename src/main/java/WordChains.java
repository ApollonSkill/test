import java.util.List;

public interface WordChains {
    List<String> getWordChains(String startWord, String endWord);
}
