import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class WordChainsImplTest {

    private WordChains wordChains;

    @Before
    public void setUp() {
        wordChains = new WordChainsImpl("C:\\Users\\ApollonSkill\\IdeaProjects\\TestWordChains\\src\\main\\resources\\dictionary.txt");
    }

    public static Object[][] getWordChains_ShouldReturnTheBestChain_Params() {
        return new Object[][] {
                {"lead", "gold", "[lead, load, goad, gold]"},
                {"ruby", "code", "[ruby, rubs, robs, rods, rode, code]"},
                {"cat", "dog", "[cat, cot, cog, dog]"}
        };

    }
    @Test
    @Parameters(method = "getWordChains_ShouldReturnTheBestChain_Params")
    public void getWordChains_ShouldReturnTheBestChain(String startWord, String endWord, String expectedResult) throws Exception {
        assertEquals(wordChains.getWordChains(startWord, endWord).toString(), expectedResult);
    }

}